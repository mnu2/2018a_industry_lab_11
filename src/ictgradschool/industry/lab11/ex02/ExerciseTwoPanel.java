package ictgradschool.industry.lab11.ex02;

import javax.lang.model.element.VariableElement;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel implements ActionListener {

    private JButton add;
    private JButton subtract;
    private JTextField no1;
    private JTextField no2;
    private JTextField result;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);
        add = new JButton("Add");
        subtract = new JButton("Subtract");
        no1 = new JTextField(10);
        no2 = new JTextField(10);
        result = new JTextField(20);

        JLabel resultLabel = new JLabel("Result:");

        this.add(no1);
        this.add(no2);
        this.add(add);
        this.add(subtract);
        this.add(resultLabel);
        this.add(result);

        add.addActionListener(this);
        subtract.addActionListener(this);
    }

    public void actionPerformed(ActionEvent e){
        double one = Double.parseDouble(no1.getText());
        double two = Double.parseDouble(no2.getText());

        if (e.getSource() == add){
            String sum = "" + (one + two);
            result.setText(sum);
        }

        else if (e.getSource() == subtract){
            String diff = "" + (one - two);
            result.setText(diff);
        }
    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}
