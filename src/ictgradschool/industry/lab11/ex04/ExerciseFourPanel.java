package ictgradschool.industry.lab11.ex04;

import com.sun.xml.internal.org.jvnet.fastinfoset.sax.EncodingAlgorithmContentHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.security.Key;
import java.util.ArrayList;

import static ictgradschool.industry.lab11.ex04.Direction.*;
import static java.awt.event.KeyEvent.*;

/**
 * Displays an animated balloon.
 */
public class ExerciseFourPanel extends JPanel implements ActionListener, KeyListener {

    private ArrayList<Balloon> balloon = new ArrayList<Balloon>();
    private  JButton moveButton;
    private Timer timer;
    private int i = 0;

    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseFourPanel() {
        setBackground(Color.white);

        balloon.add(new Balloon(30, 60));
        this.timer = new Timer(500, this);
        this.addKeyListener(this);

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()){
            case VK_UP:
                changeDirection(Up);
                moveBalloon();
                timer.start();
                break;
            case VK_LEFT:
                changeDirection(Left);
                moveBalloon();
                timer.start();
                break;
            case VK_RIGHT:
                changeDirection(Right);
                balloon.get(i).move();
                timer.start();
                break;
            case VK_DOWN:
                changeDirection(Down);
                moveBalloon();
                timer.start();
                break;
            case VK_S:
                timer.stop();
                break;
            case VK_N:
                timer.stop();
                balloon.add(new Balloon(30, 60));
                i++;
        }



        repaint();

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }


    /**
     * Moves the balloon and calls repaint() to tell Swing we need to redraw ourselves.
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        moveBalloon();



        // Sets focus to the panel itself, rather than the JButton. This way, the panel can continue to generate key
        // events even after we've clicked the button.
        requestFocusInWindow();

        repaint();
    }

    private void moveBalloon(){
        for (int i = 0; i < balloon.size(); i++){
            balloon.get(i).move();
        }
    }

    private void drawBalloon(Graphics g){
        for (int i = 0; i < balloon.size(); i++){
            balloon.get(i).draw(g);
        }
    }

    private void changeDirection(Direction direction){
        for (int i = 0; i< balloon.size(); i++){
            balloon.get(i).setDirection(direction);
        }
    }



    /**
     * Draws any balloons we have inside this panel.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawBalloon(g);
        
        // Sets focus outside of actionPerformed so key presses work without pressing the button
        requestFocusInWindow();
    }
}
