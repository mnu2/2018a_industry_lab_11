package ictgradschool.industry.lab11.ex01;

import javax.lang.model.element.VariableElement;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calculateHealthWeight;
    private JTextField height;
    private JTextField weight;
    private JTextField bmi;
    private JTextField maxWeight;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
          calculateBMIButton = new JButton("Calculate BMI");
          calculateHealthWeight = new JButton("Calculate Healthy Weight");
          height = new JTextField(5);
          weight = new JTextField(5);
          bmi = new JTextField(5);
          maxWeight = new JTextField(5);

        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel heightLabel = new JLabel("Height in metres:");
        JLabel weightLabel = new JLabel("Weight in kilograms:");
        JLabel BMILabel = new JLabel("Your Body Mass Index (BMI) is:");
        JLabel maxWeightLabel = new JLabel("Maximum Healthy Weight for your Height:");

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)
        this.add(heightLabel);
        this.add(height);
        this.add(weightLabel);
        this.add(weight);
        this.add(calculateBMIButton);
        this.add(BMILabel);
        this.add(bmi);
        this.add(calculateHealthWeight);
        this.add(maxWeightLabel);
        this.add(maxWeight);
        // TODO Add Action Listeners for the JButtons
        calculateBMIButton.addActionListener(this);
        calculateHealthWeight.addActionListener(this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {
            double h = Double.parseDouble(height.getText());
        double w = Double.parseDouble(weight.getText());

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        if (event.getSource() == calculateBMIButton){
            String  bodymass = "" + roundTo2DecimalPlaces(w/(h*h));
            bmi.setText(bodymass);
        }
        else if (event.getSource() == calculateHealthWeight){
            String healthy = "" + roundTo2DecimalPlaces(24.9*h*h);
            maxWeight.setText(healthy);
        }


    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}
